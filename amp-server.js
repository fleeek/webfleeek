/**
 * Simple server for serving fleeek amp pages.
 */
const sendinblue = require('sendinblue-api');
const mysql = require('mysql');
// For cross-origin requests
const cors = require('cors')
const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const favicon = require("serve-favicon");

// Set up app
const app = express();
app.use(cors())
app.set('port', (process.env.PORT || 2000));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(favicon(path.join(__dirname, "src", "img", "favicon.ico")));
app.set('view engine', 'ejs');

const apiResponses = {
    1: {
        "result": 0,
        "data": {
            "id": "",
            "type": "text",
            "user": {
                "id": "",
                "name": "Amethyst L",
                "avatar_url": "https://d1uxkooe25eeff.cloudfront.net/cutmypic.png",
                "link": ""
            },
            "is_draft": false,
            "created_time": 1511745927.023601,
            "title": "My new highlighter obsession - The Diorskin Nude Air Luminizer Powder",
            "content": [
                {
                    "type": "p",
                    "content": "I don't usually use pink highlighters, as I have a warmer undertone and usually reach for golden ones. However, " +
                        "as I am growly interested in cooler tone makeup looks (purple, blue, gray), I have been hunting for something that will complementing the look." +
                        "This highlighter caught my attention when I walked passed by Dior's counter in Macy's, and I was instantly amazed by how smooth and beautiful it swatches on my hand." +
                        "What's even better is it comes with a very subtle pink color that will brighten you face so much."
                },
                {
                    "type": "h2",
                    "content": "Here is a closer up look. "
                },
                {
                    "type": "p",
                    "content": "The packaging is luxious, sturdy yet lightweight, perfect for travel. It also comes with a brush, but I prefer to use my own highligher brushes."
                },
                {
                    "type": "img",
                    "content": {
                        "src": "https://d1uxkooe25eeff.cloudfront.net/blog/349066163.jpg",
                        "alt": "highlighter",
                        "aspect_ratio": 0.9}
                },
                {
                    "type": "img",
                    "content": {
                        "src": "https://dnf1hlp20dppv.cloudfront.net/blog/172550362.jpg",
                        "alt": "highlighter",
                        "aspect_ratio": 0.8}
                },
                {
                    "type": "h2",
                    "content": "And some swatches to show it in action"
                },
                {
                    "type": "p",
                    "content": "You can see the beautiful sheen it gives to the skin from the swatch. It catches the light perfectly with its complex mix of shimmer, but does not accenturate the pore."

                },
                {
                    "type": "img",
                    "content": {
                        "src": "https://s3-us-west-1.amazonaws.com/fleeekio/blog/114113520.jpg ",
                        "alt": "highlighter",
                        "aspect_ratio": 1.2}
                },
                {
                    "type": "img",
                    "content": {
                        "src": "https://s3-us-west-1.amazonaws.com/fleeekio/blog/1899352473.jpg",
                        "alt": "highlighter",
                        "aspect_ratio": 1.8}
                },
                {
                    "type": "p",
                    "content": "To me, it instantly brightens up my face and gives it a natural luminous glow."
                },
                {
                    "type": "product",
                    "content": {
                        "id":32739278397,
                        "img": "https://www.sephora.com/productimages/sku/s1901792-main-Lhero.jpg",
                        "brand": "Dior",
                        "product_name": "Diorskin Nude Air Luminizer Powder"
                    },
                },
                {
                    "type": "p",
                    "content": "I would recommend this to you if you like the following things"
                },
                {
                    "type": "ul",
                    "content": ["a cooler glow", "subtle glow", "smooth texture"]
                },
                {
                    "type": "p",
                    "content": "And are willing to splurge on a highlighter that will enhance your entire look."
                },
                {
                    "type": "p",
                    "content": "Please comment down below and tell me what you think! Love to hear from you."
                },
            ],
        }
    },
    2: {"result": 0,
        "data": {
            "id": "",
            "type": "text",
            "user": {
                "id": "",
                "name": "Amethyst L",
                "avatar_url": "https://d1uxkooe25eeff.cloudfront.net/cutmypic.png",
                "link": ""
            },
            "is_draft": false,
            "created_time": 1510474546.023601,
            "title": "Mac Lipstick in Dangerous",
            "content": [
                {
                    "type": "p",
                    "content": "This is my go-to red lipstick. It compliments my skin tone so well, and make my teeth look super white. I got asked everytime I wore it."
                },
                {
                    "type": "product",
                    "content": {
                        "id":78034098576,
                        "img": "https://www.maccosmetics.ca/media/export/cms/products/640x600/mac_sku_M0N944_640x600_0.jpg",
                        "brand": "MAC",
                        "product_name": "Retro Matte Lipstick in Dangerous"
                    },
                },
                {
                    "type": "img",
                    "content": {
                        "src": "https://d1uxkooe25eeff.cloudfront.net/blog/165851361.jpg",
                        "alt": "mac_lipstick",
                        "aspect_ratio": 1.8}
                },
                {
                    "type": "p",
                    "content": "It is the official lipstick for my graduation day and wedding day!"
                },
                {
                    "type": "img",
                    "content": {
                        "src": "https://d1uxkooe25eeff.cloudfront.net/blog/26602043.jpg",
                        "alt": "mac_lipstick",
                        "aspect_ratio": 0.65}
                },
                {
                    "type": "img",
                    "content": {
                        "src": "https://d1uxkooe25eeff.cloudfront.net/blog/1808728399.jpg",
                        "alt": "mac_lipstick",
                        "aspect_ratio": 1.5}
                },
                {
                    "type": "p",
                    "content": "Finally, it can double up as a coral shade. Just dab it on ever so slightly and blend it out! "
                },
                {
                    "type": "img",
                    "content": {
                        "src": "https://d1uxkooe25eeff.cloudfront.net/blog/290766448.jpg",
                        "alt": "mac_lipstick",
                        "aspect_ratio": 1.3}
                },

            ],
        }
    }
};

app.get('/p/:id/:title', function(req, res) {
    const id = req.params.id;
    const title = req.params.title;
    const data = apiResponses[id].data;

    // Redirect to the right page url
    if (data.title !== title) {
        res.redirect('/p/' + id + '/' + data.title)
        return;
    }
    res.render('blog-amp', {
        title: data.title,
        created_time: convertToTimeString(data.created_time),
        pageContents: data.content,
        user: data.user,
    });
});

const convertToTimeString = (timestampInSeconds) => {
    // TODO: use "Today" if the post is created today.
    // Javascript uses milliseconds timestamp.
    return new Date(timestampInSeconds * 1000).toLocaleString();
};

app.listen(app.get('port'), function () {
    console.log('Server started at port: ' + app.get('port') + '/');
});
