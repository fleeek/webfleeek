sudo apt-get install mysql-server-5.5 mysql-5.5

sudo aptitude install mysql-client

mysql -u root mysql

mysql> CREATE USER 'fleeek'@'localhost' IDENTIFIED BY 'fleeek';
Query OK, 0 rows affected (0.01 sec)

mysql> GRANT ALL PRIVILEGES ON fleeek.* TO 'fleeek'@'localhost' WITH GRANT OPTION;
Query OK, 0 rows affected (0.00 sec)

mysql> create database fleeek;
Query OK, 1 row affected (0.00 sec)

mysql> CREATE TABLE users (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, email VARCHAR(100), created_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, device VARCHAR(100));

mysql> insert into users(email, created_ts, device) values ('test@gmail.com', CURRENT_TIMESTAMP, 'mobile');

mysql> CREATE TABLE emails (user_id INT, email VARCHAR(100), created_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE);

+mysql> ALTER IGNORE TABLE users ADD UNIQUE (email);

npm install mysql