import {SocialIcon} from 'react-social-icons';

const React = require('react');
const createReactClass = require('create-react-class');

const Footer = createReactClass({
	render() {
		return (
			<div className="footer row">
				<div className="col-md-6 col-sm-6">
				</div>
				<div className="col-md-6 col-sm-6 pt-3 offset-lg-2 offset-md-2 offset-sm-1 offset-1">
                    <p>&copy; Fleeek</p>
                </div>
			</div>
		)
	}
});

export default Footer;
