import React from 'react';
import PhotoWall from './PhotoWall';

class Jumbotron extends React.Component {
  render() {
    return (
      <div className="jumbotron">
        <div className="container-fluid">
          <div className="row offset-xl-2 offset-lg-2 offset-md-2 offset-sm-1 offset-1">
            <div className="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-10 ">
              <img src="http://d1uxkooe25eeff.cloudfront.net/logo.png" className="logo"></img>
              <div className="row page-title-upper-space"></div>

              <img src="http://d1uxkooe25eeff.cloudfront.net/app_name.png" className="app-name-img"></img>
              <div className="page-title d-md-block">
                <p>Tell your unique beauty stories</p>
              </div>
              <div className="col-md-6"></div>
            </div>
          </div>
          
          <PhotoWall />
        </div>
      </div>
    );
  }
}

export default Jumbotron;
