import React from 'react';
import Email from './Email';
import Jumbotron from './Jumbotron'
import { get } from 'axios';
import Footer from './Footer';

class App extends React.Component {
  render() {
    return (
      <div className='app'>
         <Jumbotron />
         <Email onSubmit={this.onFormSubmit} />
         <Footer/>
      </div>
    );
  }

  constructor(props) {
    super(props);

    this.state = {
      email: '',
    };

    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  onFormSubmit(email) {
  	// const proxyurl = "https://cors-anywhere.herokuapp.com/";
    const url = `http://18.216.159.224:3000/api/saveUserInfo/${email}`;

    get( url, {data: {'email': email}})
        .then(({ data }) => {
          this.setState({ email: null });
        });
  }
}

export default App;
