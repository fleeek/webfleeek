import React, {Component} from 'react';

class Email extends Component {
	constructor(props) {
		super(props);

		this.state = {
			email: '',
			hideInput: false,
		};

		this.inputUpdated = this.inputUpdated.bind(this);
		this.submitEmail = this.submitEmail.bind(this);
	}

	render() {
		return (
			<div className="container-fluid">
				<div className="introduction-container row offset-lg-2 offset-md-2 offset-sm-1 offset-1 pt-lg-2">
					<div className="col-lg-4 d-lg-block mobile-img-container">
						<img src="http://dnf1hlp20dppv.cloudfront.net/mobile_img.jpg" className="mobile-img"></img>
					</div>
					<div className="col-lg-5 col-md-8 col-sm-10 col-10 email pt-lg-5 pt-md-4 pt-sm-1 pt-2 offset-lg-1">
						<div className="email-form-title">
							<h1>Be On Fleeek</h1>
							<p>Mobile experience of being a beauty blogger.</p>
						</div>
						{!this.state.hideInput &&
							<form className="email-form" onSubmit={this.submitEmail}>
								<div class="input-group">
									<input
										className="form-control col-lg-12 col-md-8 col-sm-10 col-10 mt-md-4"
										type="input"
										name="email"
										placeholder="Your email"
										value={this.state.email}
										onInput={this.inputUpdated}/>
									<button type="submit" className='btn btn-success input-group-addon mt-md-4'>
										<img src="http://dnf1hlp20dppv.cloudfront.net/submit_button.png" width="30" height="30" alt="submit" />
									</button>
								</div>
							</form>
						}

						{!this.state.hideInput &&
							<div className="email-form-footer">
								<p>Subscribe for product launch news.</p>
							</div>
						}
						{this.state.hideInput &&
							<div className="email-form-footer">
								<p>Thank you. Something exciting will happen soon!</p>
								<p>You will receive an invitation to our app once we are launched.</p>
							</div>
						}
					</div>
				</div>
			</div>
		);
	}

	inputUpdated(e) {
		const {value} = e.target;
		this.setState({email: value});
	}

	submitEmail(e) {
		e.preventDefault();

		const {email} = this.state;
		const {onSubmit} = this.props;

		onSubmit(email);
		this.setState({email: '', hideInput: true});
	}
}

export default Email;
