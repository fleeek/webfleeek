'use strict';

const React = require('react');
const createReactClass = require('create-react-class');
const Carousel = require('nuka-carousel');

const Display = createReactClass({
	mixins: [Carousel.ControllerMixin],
	render() {
		return (
			<Carousel autoplay={true}>
				<img src="https://media.giphy.com/media/1L4LhdhJVJAd2/giphy.gif"/>
				<img src="https://im4.ezgif.com/tmp/ezgif-4-a28beda28b.gif"/>
				<img src="https://im4.ezgif.com/tmp/ezgif-4-4f3eb49a0b.gif"/>
			</Carousel>
		)
	}
});

export default Display;