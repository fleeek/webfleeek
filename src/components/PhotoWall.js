
import React from 'react';
import {Motion, spring} from 'react-motion';

export default class PhotoWall extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      intervalId: 0,
      currentCount: 0,
    };

    // Get an increasing count from timer
    this.componentDidMount = this.componentDidMount.bind(this);
    this.componentWillUnmount = this.componentWillUnmount.bind(this);
    this.timer = this.timer.bind(this);

    // Set rem value
    this.getREMValue('rem-getter');

    // Import all imgs
    // this.images = this.importAllImgs(require.context('../img/photo_wall', false, /\.(png|jpe?g|svg)$/));
    this.images = {};
    let i = 0;
    while (i < 20) {
        // Use two different distributions for speed.
        if (i % 2 === 0) {
            this.images[i] = 'http://dnf1hlp20dppv.cloudfront.net/' + (i+1) + '.jpg';
        } else {
            this.images[i] = 'http://d1uxkooe25eeff.cloudfront.net/' + (i+1) + '.jpg';
        }
        i++;
    }
    this.imgKeys = Object.keys(this.images);
    this.imgSize = 14 * this.em;
    this.screenPortions = [];
    this.imgKeys.forEach(index => {
      this.screenPortions.push(index * this.imgSize);
    });
    this.screenSize = this.imgSize * (this.imgKeys.length - 1);
  }

  componentDidMount() {
    const timerInterval = 50;
    let intervalId = setInterval(this.timer, timerInterval);
    // store intervalId in the state so it can be accessed later:
    this.setState({intervalId: intervalId});
  }

  componentWillUnmount() {
     // use intervalId from the state to clear the interval
     clearInterval(this.state.intervalId);
  }

  timer() {
     // setState method is used to update the state
     this.setState({ currentCount: (this.state.currentCount + 1) % this.screenSize });
  }

  getREMValue(id) {
    var div = document.getElementById(id);
    div.style.height = `1em`;
    this.em = div.offsetHeight;
  }

  importAllImgs(r) {
    let images = {};
    r.keys().map((item, index) => { images[index] = r(item); });
    return images;
  }


  render() {
    const currentCount = this.state.currentCount;

    // Calculate the x translate value
    const getXPosition = (currentStep, initialImgPosition) => {
      if ((currentStep + initialImgPosition) % this.screenSize > initialImgPosition ) {
        return currentStep % this.screenSize ;
      } else {
        return - this.imgSize - (initialImgPosition - (currentStep + initialImgPosition) % this.screenSize);
      }
    }


    // Function to create a list of img divs for the photo wall
    const listItems = (currentStep) => 
      {return this.imgKeys.map((item) =>
        <div id={item} className="img-container">
          <img src={this.images[item]} className="img-responsive" style={{
            transform: `translate3d(${getXPosition(currentStep, this.screenPortions[item])}px, 0, 0)`,
          }}></img>
        </div>
        );
    };

    // Impirical value
    const speedFactor = 1.5 
    
    return (
      <div>
        <Motion style={{currentStep: currentCount * speedFactor}}>
          {({currentStep}) =>
            // children is a callback which should accept the current value of
            // `style`
            <div className="picture-wall">
              {listItems(currentStep)}
            </div>
          }
        </Motion>
      </div>
    );
  };
}
