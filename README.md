# fleeek.io
Website of fleeek

The skeleton is taken from companion code for the Beginner React series on the [Scotch School](https://school.scotch.io/).

## Setup

install the dependencies, set up the local database and run the project.

Please see instruction on database_setup.sh to setup the database (sorry the script is not automated yet).

To run the project, do

```
yarn
yarn start
```
or 

``` 
npm start
```

Then open up your browser to http://localhost:8080, and you will be able to see the project.
