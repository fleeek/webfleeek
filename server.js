/**
 * Simple server for receiving client request and writing to fleeek db.
 */
const sendinblue = require('sendinblue-api');
const mysql = require('mysql');
// For cross-origin requests
const cors = require('cors')
const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const favicon = require("serve-favicon");

// Set up app
const app = express();
app.use(cors())
app.set('port', (process.env.PORT || 3000));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// TODO: encrpt database credentials and read from the encrypted keystore
const pool  = mysql.createPool({
    connectionLimit : 10,
    host: "localhost",
    user: "fleeek",
    password: "fleeek",
    database: "fleeek",
});

app.get('/api/saveUserInfo/:email', function (req, res) {
	const request_email = req.params.email;

	const saveUserEmailQuery = `insert into users(email, created_ts, device) values ('${request_email}', CURRENT_TIMESTAMP, 'mobile') on duplicate key update created_ts = CURRENT_TIMESTAMP;`;
    pool.getConnection(function(err, con) {
        if (err) {
			console.log('Fail to connect to database.')
			throw err;
        }

        // Use the connection
        con.query(saveUserEmailQuery, function (err, result) {
            // And done with the connection.
            con.release();

            // Handle error after the release.
            if (err) throw err;

            console.log('successfully updated user email.');
            // Don't use the connection here, it has been returned to the pool.
        });
    });

	// FIXME API Key should be encrypted
	var parameters = { "apiKey": "UBTWCOYkNxb9E1Jz", "timeout": 5000 };
	var sendinObj = new sendinblue(parameters);

	let toObj = {};
	toObj[request_email] = "to";

	let data = {
		"to": toObj,
		"from": ["welcome@fleeek.io", "the Fleeek Team"],
		"replyto": ["welcome@fleeek.io", "reply to"],
		"subject": "YOU'RE ON THE LIST!",
		"html": "<div>Thank you for signing up to learn more about our upcoming product, Fleeek!\n" +
		"<br>\n" +
		"<br>\n" +
		"<strong><span class=\"il\">Fleeek is created with a singular mission: to inspire individuals to express their unique beauty.</span></strong>\n" +
		"We hope to reinvent the way people share their beauty experience, making it so easy, so effective and so interesting.\n" +
		"<br>\n" +
		"<br>\n" +
		"With Fleeek, everyone can create how-to videos or mini-blogs in just a few minutes. As a mobile publishing platform, it helps your little stories get out to the rest of the world, and lets you discover others' stories.<br>\n" +
		"<br>\n" +
		"<strong><span class=\"il\">Fleeek will launch soon.</span></strong> Once the app is available, we will send you an invitation to download the app and to join our lively community of beauty inspirers, people who are just like you.<br>\n" +
		"<br>\n" +
		"Can't wait to see you <strong><span class=\"il\">on Fleeek</span></strong>!" +
		"<br><br><br>Cheers,<br>" +
		"The Fleeek Team",
		"headers": {"Content-Type": "text/html;charset=iso-8859-1", "X-param1": "value1"},
	};

	sendinObj.send_email(data, function(err, response) {
		if(err){
			console.log("Email send error: " + err);
		} else {
			console.log(response);
			const emailQuery = `INSERT INTO emails(user_id, email, created_ts) values ((SELECT id FROM users WHERE email = '${request_email}'), '${request_email}', CURRENT_TIMESTAMP);`;
            pool.getConnection(function(err, con) {
                if (err) {
                    console.log('Fail to connect to database.')
                    throw err;
                }

                // Use the connection
                con.query(emailQuery, function (err, result) {
                    // And done with the connection.
                    con.release();

                    // Handle error after the release.
                    if (err) throw err;
                    console.log('successfully saved the email record.');
                });
            });
		}
	});
});

app.listen(app.get('port'), function () {
	console.log('Server started at port: ' + app.get('port') + '/');
});
