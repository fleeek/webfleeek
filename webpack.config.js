                                                                                                                                           const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const {resolve} = require('path');

module.exports = {
	devtool: 'cheap-module-eval-source-map',
	entry: [
		'webpack-dev-server/client?http://0.0.0.0:80',
		'./src/app.js'
		//resolve(__dirname, 'src', 'app.js'),
	],
	output: {
		filename: '[name].[hash].js',
		path: resolve(__dirname, 'build')
	},
	devServer: {
		compress: true,
		disableHostCheck: true,
	},
	module: {
		rules: [
			{
				test: /\.jsx?$/,
				exclude: /node_modules/,
				use: ['babel-loader']
			},
			{
				test: /\.s?css$/,
				use: [
					'style-loader',
					'css-loader?&camelCase&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]',
					'sass-loader?'
				]
			},
			{
				test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
				loader: 'url-loader',
				options: {
					limit: 10000
				}
			},
            {
                test: /\.ico$/,
                loader: 'file-loader?name=[name].[ext]'  // <-- retain original file name
            },
		]
	},
	plugins: [
		new webpack.NamedModulesPlugin(),
		new HtmlWebpackPlugin({
			template: resolve(__dirname, 'src', 'index.html'),
            favicon: 'src/img/favicon.ico'
		}),
    new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
        Popper: ['popper.js', 'default'],
        // In case you imported plugins individually, you must also require them here:
        Util: "exports-loader?Util!bootstrap/js/dist/util",
        Dropdown: "exports-loader?Dropdown!bootstrap/js/dist/dropdown",
      })
	]
}
